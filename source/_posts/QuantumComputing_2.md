---
title : Learning on quantum computing(2)
date : 2022-02-16
categories : quantum computing
mathjax : true
---

I start with the textbook ["Mastering quantum computing with IMB QX"](https://github.com/PacktPublishing/Mastering-Quantum-Computing-with-IBM-QX).

# Quantum Circuit

## Quantum Circuit Diagram
![Quantum circuit diagram](qc_diagram_bell.png)

The above diagram is a quantum ciucuit diagram. The "H" means Hadamard gate and "X" means the CNOT gate."M" means measurement.

## Generate Quantum Circuit by Qiskit(Quantum Information Science Kit)

### Single qubit circuit

{% codeblock lang=python %}
from qiskit import QuantumCircuit, QuantumRegister, ClassicalRegister
qr = QuantumRegister(1)
circuit = QuantumCircuit(qr)
circuit.h(qr[0])
circuit.x(qr[0])
{% endcodeblock %}

The parameter "1" means the quantum register contains one qubit.

### About QuantumCircuit class and gates

|gate	|naming in QuantumCircuit class|
|---	|---|
|I	|iden|
|X	|x|
|Y	|y|
|Z	|z|
|H	|h|
|S	|s|
|$S^{'}$|sdg|
|T	|t|
|$T^{'}$|tdg|
|CNOT	|cx|

### Multi-qubit gate in qiskit

{% codeblock lang=python %}
qr = QuantumRegister(2)
circuit = QuantumCircuit(qr)
circuit.h(qr[0])
circuit.cx(qr[0],qr[1])
{% endcodeblock %}

### Classical register in qiskit
### Measurement in qiskit

{% codeblock lang=python %}
qr = QuantumRegister(2)
cr = ClassicalRegister(2)
circuit = QuantumCircuit(qr, cr)
circuit.h(qr[0])
circuit.cx(qr[0],qr[1])
circuit.measure(qr, cr)
{% endcodeblock %}

## Usefull quantum circuit

### prepare any binary input with X gate

$$
\begin{cases}
|00>=|0>\otimes|0>\\\\
|01>=|0>\otimes X|0>\\\\
|10>=X|0>\otimes |0>\\\\
|11>=X|0>\otimes X|0>\\\\
\end{cases}
$$

### Swap the two qubits

{% codeblock lang=python %}
circuit.cx(qr[0],qr[1])
circuit.cx(qr[1],qr[0])
circuit.cx(qr[0],qr[1])
{% endcodeblock %}

![swap two qubits](swap.png)
