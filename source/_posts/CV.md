---
title : CV of Yu Zhang
date : 2021-11-16
categories : CV
mathjax : true
---

<center>Yu Zhang</center>

---
# Personal Information
+ gender : male
+ birth : Feb 17th, 1991
+ mail : yu.zhang@cern.ch
+ skype : yu.zhang0217

---
# Education

+ Aug 2010 - June 2014, Bachelor, Nuclear and Particle Physics, University of Science and Technology of China, Hefei
+ Aug 2014 - May 2020, PhD, Nuclear and Particle Physics, Institute of High Energy Physics, Chinese Academy of Sciences, Beijing

# Employment

+ July 2020 - now, Postdoc, Fudan University, Shanghai
+ March 2022 - March 2024, China-Germany Postdoc Exchange Program, DESY, Hamburg

# Research Experience

+ VBF Higgs$\rightarrow\gamma\gamma$ on ATLAS experiment
+ HH$\rightarrow\gamma\gamma WW$ on ATLAS experiment
+ HH$\rightarrow WWWW$ on ATLAS experiment
+ SM $t\bar{t}t\bar{t}\rightarrow \ge 1\tau$ on CMS experiment
+ MET performance on CMS experiment

# Skills

# Conference talks and posters

# Publications

